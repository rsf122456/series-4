package sbu.cs.parser.json;

import java.util.*;

public class Json implements JsonInterface {

    protected List<KeysAndValues> keysAndValues = new ArrayList<>();

    public Json(List<KeysAndValues> keysValues)
    {
        this.keysAndValues = keysValues;
    }
    @Override
    public String getStringValue(String key) {
        for (int i = 0; i < keysAndValues.size(); i++){
            if (keysAndValues.get(i).getKey().equals(key)) {
                return keysAndValues.get(i).getValue();
            }
        }
        return null;
    }
}
