package sbu.cs.parser.json;

import java.util.*;

public class JsonParser {

    protected static List<KeysAndValues> keysAndValues = new ArrayList<>();
    /*
    * this function will get a String and returns a Json object
     */
    public static Json parse(String data) {
        String Data = data.replaceAll("[{}\"]","");
        //we have a problem for split with ',' in second testcase.
        StringBuilder DataName = new StringBuilder(Data);
        if(Data.contains("[")){
            for (int i = DataName.indexOf("["); DataName.charAt(i) != ']'; i++){
                if (DataName.charAt(i + 1) == ','){
                    DataName.setCharAt(i + 1,'#');
                }
            }
        }
        String[] firstSplit = DataName.toString().split(",");
        for (int i = 0; i < firstSplit.length; i++){
            String[] secondSplit = firstSplit[i].split(":");
            /*
            we know that our nameChanges just have two room
            the first one is our key of name[i]
            and the second one is our value
             */
            secondSplit[0] = secondSplit[0].trim().replaceAll("#",",");
            secondSplit[1] = secondSplit[1].trim().replaceAll("#",",");
            KeysAndValues MyJsonKeyValue = new KeysAndValues(secondSplit[0],secondSplit[1]);
            keysAndValues.add(MyJsonKeyValue);
        }
        return new Json(keysAndValues);
    }

    /*
    * this function is the opposite of above function. implementing this has no score and
    * is only for those who want to practice more.
     */
    public static String toJsonString(Json data) {
        return null;
    }
}
