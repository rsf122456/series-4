package sbu.cs.parser.json;

public class KeysAndValues {
    protected String key;
    protected String value;
    /*
     implement constructor,
     pay attention : we don't implement setter function for out private elements
     we use constructor instead.
     */
    public KeysAndValues(String key, String value) {
        this.key = key;
        this.value = value;
    }
    /*
    because our key and value are private,
    we should implement getter function for them
     */
    public String getKey(){
        return key;
    }
    public String getValue(){
        return value;
    }
}
